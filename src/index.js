window.addEventListener('load', function () {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function () {
        this.response.bikes.forEach(function (bike) {
            var wrap = Utils.creteElement('div', document.body, 'bike-wrap');
            new Bike(bike, wrap);
        });
    });
    xhr.open('get', './data/bikes.json');
    xhr.responseType = 'json';
    xhr.send();
});