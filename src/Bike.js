function Bike(bike, parentElement) {
    this.bikeId = bike.id;
    this.body = parentElement;
    var div = Utils.creteElement('h3', this.body), a = Utils.creteElement('a', div);
    a.textContent = bike.vendor + ' ' + bike.name;
    a.href = '#';
    a.addEventListener('click', function (e) {
        e.preventDefault();
        this.load();
    }.bind(this));
}

Bike.prototype.load = function () {
    if (this.status == 'loading' || this.status == 'loaded') {
        return;
    }

    var ul = Utils.creteElement('ul', this.body, 'tab-menu');
    this.status = 'loading';
    var data = Utils.creteElement('div', this.body, 'bike-data');
    var loading = Utils.creteElement('div', data, 'bike-loading');

    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function (event) {
        var bike = event.target.response;
        data.removeChild(loading);
        this.status = 'loaded';

        var li;
        if (Array.isArray(bike.images) && bike.images.length > 0) {
            li = Utils.creteElement('li', ul, 'tab');
            li.textContent = 'Images (' + bike.images.length + ')';
            li.addEventListener('click', function (event) {
                this._activateMenu(ul, event.target);
                data.innerHTML = '';
                bike.images.forEach(function (image) {
                    var img = Utils.creteElement('img', data);
                    img.src = './data/bike/' + bike.id + '/image/' + image;
                    img.style.width = '300px'
                });
            }.bind(this));
        }
        li = Utils.creteElement('li', ul, 'tab');
        li.textContent = 'Characteristics';
        li.addEventListener('click', function (event) {
            this._activateMenu(ul, event.target);
            data.innerHTML = '';
            Object.keys(bike.characteristics).forEach(function(level1) {
                var h2 = Utils.creteElement('h2', data);
                h2.style.paddingLeft = '10px';
                h2.textContent = level1;

                Object.keys(bike.characteristics[level1]).forEach(function(level2) {
                    var h3 = Utils.creteElement('h3', data);
                    h3.style.paddingLeft = '20px';
                    h3.textContent = level2;

                    var ul = Utils.creteElement('ul', data);
                    ul.style.paddingLeft = '30px';
                    ul.style.listStyleType = 'none';

                    Object.keys(bike.characteristics[level1][level2]).forEach(function(level3) {
                        var li = Utils.creteElement('li', ul);
                        li.innerHTML = '<b>' + level3 + '</b>: ' +  bike.characteristics[level1][level2][level3];
                    });
                });
            });
        }.bind(this));

        ul.querySelector('li').click();
    }.bind(this));
    xhr.open('get', './data/bike/' + this.bikeId + '.json');
    xhr.responseType = 'json';
    xhr.send();
};

Bike.prototype._activateMenu = function (menu, item) {
    Array.prototype.forEach.call(menu.querySelectorAll('li'), function (li) {
        li.classList.remove('active');
    });
    item.classList.add('active');
};