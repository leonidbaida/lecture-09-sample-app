var fs = require('fs-extra');
var cc = require('closure-compiler');
var glob = require('glob');
var CleanCSS = require('clean-css');

function emptyTargetDir(cb) {
    console.log('Emptying started...');
    fs.emptyDir('./dist', function () {
        console.log('Emptying finished!');
        cb();
    });
}

function copyResources(cb) {
    console.log('Copying started...');
    fs.copy('./src', './dist', function () {
        console.log('Copying finished!');
        cb();
    });
}

function closureCompiler(cb) {
    console.log('CC started...');
    var jsFiles = glob.sync('./dist/**/*.js'), jsFilesProcessed = 0;
    jsFiles.forEach(function (file) {
        console.log('    CC start: ' + file);
        cc.compile(fs.readFileSync(file), {}, function (err, stdout) {
            fs.writeFile(file, stdout, function () {
                console.log('    CC finished: ' + file);
                if (++jsFilesProcessed == jsFiles.length) {
                    console.log('CC finished!');
                    cb();
                }
            });
        });
    });
}

function compressJSON(cb) {
    console.log('Compressing JSON started...');
    var jsonFiles = glob.sync('./dist/**/*.json'), jsonFilesProcessed = 0;
    jsonFiles.forEach(function (file) {
        console.log('    Compressing JSON start: ' + file);
        fs.readJSON(file, function (err, json) {
            fs.writeJSON(file, json, {spaces: null}, function () {
                console.log('    Compressing JSON finished: ' + file);
                if (++jsonFilesProcessed == jsonFiles.length) {
                    console.log('Compressing JSON finished!');
                    cb();
                }
            });
        });
    });
}

function clearCSS(cb) {
    console.log('Clearing CSS started...');
    var cssFiles = glob.sync('./dist/**/*.css'), cssFilesProcessed = 0;
    cssFiles.forEach(function (file) {
        console.log('    Clearing CSS start: ' + file);
        fs.readFile(file, function (err, content) {
            new CleanCSS().minify(content, function (errors, minified) {
                fs.writeFile(file, minified.styles, function () {
                    console.log('    Clearing CSS finished: ' + file);
                    if (++cssFilesProcessed == cssFiles.length) {
                        console.log('Clearing CSS finished!');
                        cb();
                    }
                });
            });
        });
    });
}

function build() {
    console.log('build started...');
    emptyTargetDir(
        function () {
            copyResources(
                function () {
                    closureCompiler(
                        function () {
                            compressJSON(
                                function () {
                                    clearCSS(
                                        function () {
                                            console.log('build finished!');
                                        }
                                    )
                                }
                            )
                        }
                    )
                }
            )
        }
    );
}

build();